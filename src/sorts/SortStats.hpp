#pragma once

#include <cstddef>

namespace sorts {
namespace detail {
class SortStats final {
public:
  using Self = SortStats;
  using ComparisonsCount = std::size_t;
  using AssignmentsCount = std::size_t;

public:
  inline SortStats() = default;
  ~SortStats() = default;

  inline SortStats(ComparisonsCount cmp, AssignmentsCount assgn)
      : m_cmp{cmp}, m_assgn{assgn} {}

  inline ComparisonsCount comparisons() const { return m_cmp; }
  inline AssignmentsCount assignments() const { return m_assgn; }

  inline void inc_cmp() { ++m_cmp; }
  inline void inc_assgn() { ++m_assgn; }
  inline void inc_cmp(const ComparisonsCount count) { m_cmp += count; }
  inline void inc_assgn(const AssignmentsCount count) { m_assgn += count; }

  inline Self& operator+=(const Self& rhs) {
    m_cmp += rhs.m_cmp;
    m_assgn += rhs.m_assgn;
    return *this;
  }

private:
  ComparisonsCount m_cmp{0};
  AssignmentsCount m_assgn{0};
};
} // namespace detail
} // namespace sorts
