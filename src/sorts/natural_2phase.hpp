#pragma once

#include <bits/iterator_concepts.h>
#include <cstddef>
#include <functional>
#include <iterator>
#include <vector>

#include "SortStats.hpp"
#include "utils.hpp"

namespace sorts {
template <
    std::forward_iterator It,
    typename Cmp =
        std::less_equal<typename std::iterator_traits<It>::value_type>,
    typename Tape = std::vector<typename std::iterator_traits<It>::value_type>>
detail::SortStats natural_2phase(const It first, const It last,
                                 Cmp cmp = Cmp{}) {
  detail::SortStats stats{};
  const auto seq_size = std::distance(first, last);
  Tape tape(seq_size);
  for (;;) {
    auto t1e = tape.begin();
    auto t2e = tape.rbegin();
    stats += detail::distribute_series(first, last, t1e, t2e, cmp);

    auto t1 = tape.begin();
    auto t2 = tape.rbegin();
    std::size_t series{0};
    for (auto out = first; out != last; ++series) {
      stats += detail::merge_single_series(t1, t1e, t2, t2e, out, cmp);
    }
    if (series == 1) {
      break;
    }
  }
  return stats;
}
} // namespace sorts
