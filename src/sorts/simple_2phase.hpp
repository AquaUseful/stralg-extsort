#pragma once

#include <algorithm>
#include <bits/iterator_concepts.h>
#include <cstddef>
#include <functional>
#include <iterator>
#include <utility>
#include <vector>

#include "SortStats.hpp"
#include "utils.hpp"

namespace sorts {
template <
    std::forward_iterator It,
    typename Cmp = std::less<typename std::iterator_traits<It>::value_type>,
    typename Tape = std::vector<typename std::iterator_traits<It>::value_type>>
detail::SortStats simple_2phase(const It first, const It last,
                                const Cmp cmp = Cmp{}) {
  detail::SortStats stats{};
  const auto seq_size = std::distance(first, last);
  Tape tape(seq_size);
  for (std::ptrdiff_t sorted_size{1}; sorted_size < seq_size;
       sorted_size *= 2) {
    stats += detail::distribute_strides(first, last, tape.begin(),
                                        tape.rbegin(), sorted_size);
    auto t1 = tape.begin();
    auto t2 = tape.rbegin();
    auto out = first;
    for (std::ptrdiff_t i{seq_size}; i > 0; i -= sorted_size) {
      stats += detail::merge_n(t1, t2, out, std::min(sorted_size, i),
                               std::min(sorted_size, i -= sorted_size), cmp);
    }
  }
  return stats;
}
} // namespace sorts
