#pragma once

#include <algorithm>
#include <bits/iterator_concepts.h>
#include <concepts>
#include <cstddef>
#include <functional>
#include <iterator>

#include "SortStats.hpp"

namespace sorts {
namespace detail {
template <
    std::input_iterator IIt1, std::input_iterator IIt2,
    std::output_iterator<typename std::iterator_traits<IIt1>::value_type> OIt,
    std::integral Size,
    typename Comp = std::less<typename std::iterator_traits<IIt1>::value_type>>
inline SortStats merge_n(IIt1& input1, IIt2& input2, OIt& out, Size n1, Size n2,
                         Comp comp = Comp{}) {
  SortStats stats{};
  for (; n1 > 0; ++out) {
    if (n2 <= 0) {
      for (; n1 > 0; --n1) {
        *out = *input1;
        ++input1;
        ++out;
      }
      stats.inc_assgn(n1);
      return stats;
    }
    if (comp(*input1, *input2)) {
      *out = *input1;
      ++input1;
      --n1;
    } else {
      *out = *input2;
      ++input2;
      --n2;
    }
    stats.inc_cmp();
    stats.inc_assgn();
  }
  for (; n2 > 0; --n2) {
    *out = *input2;
    ++input2;
    ++out;
  }
  stats.inc_assgn(n2);
  return stats;
}

template <
    std::input_iterator IIt,
    std::output_iterator<typename std::iterator_traits<IIt>::value_type> OIt1,
    std::output_iterator<typename std::iterator_traits<IIt>::value_type> OIt2,
    typename Size = std::size_t>
inline SortStats distribute_strides(IIt ifirst, IIt ilast, OIt1 out1, OIt2 out2,
                                    const Size stride = 1) {
  SortStats stats{};
  while (ifirst != ilast) {
    for (Size i{0}; (i < stride) && (ifirst != ilast); ++i) {
      *out1 = *ifirst;
      ++ifirst;
      ++out1;
      stats.inc_assgn();
    }
    for (Size i{0}; (i < stride) && (ifirst != ilast); ++i) {
      *out2 = *ifirst;
      ++ifirst;
      ++out2;
      stats.inc_assgn();
    }
  }
  return stats;
}

template <
    std::input_iterator IIt,
    std::output_iterator<typename std::iterator_traits<IIt>::value_type> OIt,
    typename Cmp = std::less<typename std::iterator_traits<IIt>::value_type>>
inline SortStats copy_single_series(IIt& first, IIt last, OIt& out,
                                    Cmp cmp = Cmp{}) {
  SortStats stats{};
  if (first == last) {
    return stats;
  }
  IIt next = std::next(first);
  for (;;) {
    stats.inc_assgn();
    *out = *first;
    ++out;
    stats.inc_cmp();
    if ((next == last) || !cmp(*first, *next)) {
      ++first;
      return stats;
    }
    ++first;
    ++next;
  }
}

template <
    std::input_iterator IIt,
    std::output_iterator<typename std::iterator_traits<IIt>::value_type> OIt1,
    std::output_iterator<typename std::iterator_traits<IIt>::value_type> OIt2,
    typename Pred = std::less<typename std::iterator_traits<IIt>::value_type>>
inline SortStats distribute_series(IIt ifirst, IIt ilast, OIt1& out1,
                                   OIt2& out2, Pred pred = Pred{}) {
  SortStats stats{};
  if (ifirst == ilast) {
    return stats;
  }
  bool out_flag{true};
  while (ifirst != ilast) {
    if (out_flag) {
      stats += copy_single_series(ifirst, ilast, out1, pred);
    } else {
      stats += copy_single_series(ifirst, ilast, out2, pred);
    }
    out_flag = !out_flag;
  }
  return stats;
}

template <
    std::input_iterator IIt1, std::input_iterator IIt2,
    std::output_iterator<typename std::iterator_traits<IIt1>::value_type> OIt,
    typename Cmp = std::less<typename std::iterator_traits<IIt1>::value_type>>
inline SortStats merge_single_series(IIt1& ifirst1, IIt1 ilast1, IIt2& ifirst2,
                                     IIt2 ilast2, OIt& out, Cmp cmp = Cmp{}) {
  SortStats stats{};
  if (ifirst1 == ilast1) {
    stats += copy_single_series(ifirst2, ilast2, out, cmp);
    return stats;
  } else if (ifirst2 == ilast2) {
    stats += copy_single_series(ifirst1, ilast1, out, cmp);
    return stats;
  }

  IIt1 inext1 = std::next(ifirst1);
  IIt2 inext2 = std::next(ifirst2);
  for (;;) {
    stats.inc_cmp(2);
    stats.inc_assgn();
    if (cmp(*ifirst1, *ifirst2)) {
      *out = *ifirst1;
      if ((inext1 == ilast1) || !cmp(*ifirst1, *inext1)) {
        ++ifirst1;
        stats += copy_single_series(ifirst2, ilast2, ++out, cmp);
        return stats;
      }
      ++inext1;
      ++ifirst1;
    } else {
      *out = *ifirst2;
      if ((inext2 == ilast2) || !cmp(*ifirst2, *inext2)) {
        ++ifirst2;
        stats += copy_single_series(ifirst1, ilast1, ++out, cmp);
        return stats;
      }
      ++inext2;
      ++ifirst2;
    }
    ++out;
  }
}

} // namespace detail
} // namespace sorts
