#pragma once

#include <algorithm>
#include <bits/iterator_concepts.h>
#include <concepts>
#include <cstddef>
#include <functional>
#include <iterator>

#include "SortStats.hpp"
#include "utils.hpp"

namespace sorts { // namespace detail

template <
    std::forward_iterator It,
    typename Cmp = std::less<typename std::iterator_traits<It>::value_type>,
    typename Tape = std::vector<typename std::iterator_traits<It>::value_type>>
inline detail::SortStats simple_1phase(It first, It last, Cmp cmp = Cmp{}) {
  detail::SortStats stats{};
  const auto seq_size = std::distance(first, last);
  Tape tape1(seq_size), tape2(seq_size);
  detail::distribute_strides(first, last, tape1.begin(), tape1.rbegin());
  for (std::ptrdiff_t sorted_size{1}; sorted_size < seq_size;) {
    {
      auto t11 = tape1.begin();
      auto t12 = tape1.rbegin();
      auto t21 = tape2.begin();
      auto t22 = tape2.rbegin();
      for (std::ptrdiff_t i{seq_size}; i > 0;) {
        stats += detail::merge_n(t11, t12, t21, std::min(sorted_size, i),
                                 std::min(sorted_size, i -= sorted_size), cmp);
        i -= sorted_size;
        stats += detail::merge_n(t11, t12, t22, std::min(sorted_size, i),
                                 std::min(sorted_size, i -= sorted_size), cmp);
        i -= sorted_size;
      }
    }
    sorted_size *= 2;
    if (sorted_size >= seq_size) {
      std::copy(tape2.begin(), tape2.end(), first);
      stats.inc_assgn(seq_size);
      return stats;
    }
    {
      auto t11 = tape1.begin();
      auto t12 = tape1.rbegin();
      auto t21 = tape2.begin();
      auto t22 = tape2.rbegin();
      for (std::ptrdiff_t i{seq_size}; i > 0;) {
        stats += detail::merge_n(t21, t22, t11, std::min(sorted_size, i),
                                 std::min(sorted_size, i -= sorted_size), cmp);
        i -= sorted_size;
        stats += detail::merge_n(t21, t22, t12, std::min(sorted_size, i),
                                 std::min(sorted_size, i -= sorted_size), cmp);
        i -= sorted_size;
      }
    }
    sorted_size *= 2;
  }
  std::copy(tape1.begin(), tape1.end(), first);
  stats.inc_assgn(seq_size);
  return stats;
}

} // namespace sorts