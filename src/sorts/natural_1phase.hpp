#pragma once

#include <algorithm>
#include <bits/iterator_concepts.h>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <iterator>

#include "SortStats.hpp"
#include "utils.hpp"

namespace sorts {
template <
    std::forward_iterator It,
    typename Cmp =
        std::less_equal<typename std::iterator_traits<It>::value_type>,
    typename Tape = std::vector<typename std::iterator_traits<It>::value_type>>
detail::SortStats natural_1phase(const It first, const It last,
                                 Cmp cmp = Cmp{}) {
  detail::SortStats stats{};
  const auto seq_size = std::distance(first, last);
  Tape tape1(seq_size), tape2(seq_size);
  auto t11e = tape1.begin();
  auto t12e = tape1.rbegin();
  stats += detail::distribute_series(first, last, t11e, t12e, cmp);
  for (;;) {
    auto t11 = tape1.begin();
    auto t12 = tape1.rbegin();
    auto t21e = tape2.begin();
    auto t22e = tape2.rbegin();
    std::size_t series{0};
    for (bool out_flag{true};; out_flag = !out_flag) {
      if (out_flag) {
        stats += detail::merge_single_series(t11, t11e, t12, t12e, t21e, cmp);
      } else {
        stats += detail::merge_single_series(t11, t11e, t12, t12e, t22e, cmp);
      }
      ++series;
      if ((t11 == t11e) && (t12 == t12e)) {
        break;
      }
    }
    if (series == 1) {
      stats.inc_assgn(seq_size);
      std::copy(tape2.begin(), tape2.end(), first);
      return stats;
    }
    auto t21 = tape2.begin();
    auto t22 = tape2.rbegin();
    t11e = tape1.begin();
    t12e = tape1.rbegin();
    for (bool out_flag{true};; out_flag = !out_flag) {
      if (out_flag) {
        stats += detail::merge_single_series(t21, t21e, t22, t22e, t11e, cmp);
      } else {
        stats += detail::merge_single_series(t21, t21e, t22, t22e, t12e, cmp);
      }
      ++series;
      if ((t21 == t21e) && (t22 == t22e)) {
        break;
      }
    }
    if (series == 1) {
      stats.inc_assgn(seq_size);
      std::copy(tape1.begin(), tape1.end(), first);
      return stats;
    }
  }
}
} // namespace sorts
