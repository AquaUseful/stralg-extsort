#pragma once

#include <cstdint>
#include <forward_list>
#include <limits>
#include <memory>
#include <qmainwindow.h>
#include <qobjectdefs.h>
#include <qwidget.h>
#include <random>
#include <utility>
#include <vector>

#include "SortStats.hpp"
#include "ui_Main.h"

namespace ui {
class Main : public QMainWindow {
  Q_OBJECT
public:
  using Ui = Ui::MainFormUi;
  using UiPtr = std::unique_ptr<Ui>;

  using Value = std::int32_t;
  using Rdev = std::random_device;
  using Rgen = std::mt19937;
  using Array = std::forward_list<Value>;
  using Size = std::size_t;
  using Rdist = std::uniform_int_distribution<Value>;

public:
  explicit Main(QWidget* = nullptr);
  virtual ~Main() = default;

private:
  void update_array();
  void update_simple2();
  void update_simple1();
  void update_natural2();
  void update_natural1();
  void display_sort_info(int, sorts::detail::SortStats, long, bool);
  void clear_sort_info(int);
  void config_slots();

private slots:
  void update_sort_info();
  void update_arr_size();

private:
  UiPtr m_ui{std::make_unique<Ui>()};
  Array m_array{};
  Size m_arr_size{};

  Rdev m_rdev{};
  Rgen m_rgen{};

  const static Value min_val{std::numeric_limits<Value>::min()};
  const static Value max_val{std::numeric_limits<Value>::max()};
};
} // namespace ui
