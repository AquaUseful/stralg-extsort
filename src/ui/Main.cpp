#include "Main.hpp"

#include <functional>
#include <qwidget.h>

#include "SortStats.hpp"
#include "Timer.hpp"
#include "natural_1phase.hpp"
#include "natural_2phase.hpp"
#include "simple_1phase.hpp"
#include "simple_2phase.hpp"

ui::Main::Main(QWidget* parent) : QMainWindow(parent) {
  m_ui->setupUi(this);
  m_ui->retranslateUi(this);
  config_slots();
}

void ui::Main::clear_sort_info(int row) {
  for (int col{1}; col <= 3; ++col) {
    m_ui->resultsTable->item(row, col)->setText("");
  }
  m_ui->resultsTable->item(row, 4)->setCheckState(Qt::CheckState::Unchecked);
}

void ui::Main::display_sort_info(int row, sorts::detail::SortStats stats,
                                 long duration, bool is_sorted) {
  m_ui->resultsTable->item(row, 1)->setText(
      QString::number(stats.comparisons()));
  m_ui->resultsTable->item(row, 2)->setText(
      QString::number(stats.assignments()));
  m_ui->resultsTable->item(row, 3)->setText(QString::number(duration));

  const Qt::CheckState check_state =
      is_sorted ? Qt::CheckState::Checked : Qt::CheckState::Unchecked;
  m_ui->resultsTable->item(row, 4)->setCheckState(check_state);
}

void ui::Main::update_array() {
  m_array.resize(m_arr_size);
  Rdist dist(min_val, max_val);
  std::generate(m_array.begin(), m_array.end(),
                [this, &dist]() { return dist(m_rgen); });
}

void ui::Main::update_simple2() {
  auto arr = m_array;
  const auto result = timer::Timer::measure(
      sorts::simple_2phase<typename decltype(arr)::iterator>, arr.begin(),
      arr.end(), std::less<Value>{});
  display_sort_info(0, result.func_result, result.duration.count(),
                    std::is_sorted(arr.begin(), arr.end()));
}

void ui::Main::update_simple1() {
  auto arr = m_array;
  const auto result = timer::Timer::measure(
      sorts::simple_1phase<typename decltype(arr)::iterator>, arr.begin(),
      arr.end(), std::less<Value>{});
  display_sort_info(1, result.func_result, result.duration.count(),
                    std::is_sorted(arr.begin(), arr.end()));
}

void ui::Main::update_natural2() {
  auto arr = m_array;
  const auto result = timer::Timer::measure(
      sorts::natural_2phase<typename decltype(arr)::iterator>, arr.begin(),
      arr.end(), std::less_equal<Value>{});
  display_sort_info(2, result.func_result, result.duration.count(),
                    std::is_sorted(arr.begin(), arr.end()));
}

void ui::Main::update_natural1() {
  auto arr = m_array;
  const auto result = timer::Timer::measure(
      sorts::natural_1phase<typename decltype(arr)::iterator>, arr.begin(),
      arr.end(), std::less_equal<Value>{});
  display_sort_info(3, result.func_result, result.duration.count(),
                    std::is_sorted(arr.begin(), arr.end()));
}

void ui::Main::update_arr_size() {
  m_arr_size = m_ui->arrSize->value();
  const std::size_t array_kbytes = (sizeof(Value) * m_arr_size) / 1024;
  QString kbytes;
  if (array_kbytes == 0) {
    kbytes = "<1";
  } else {
    kbytes = QString::number(array_kbytes);
  }
  m_ui->arrKbytes->setText(kbytes);
}

void ui::Main::update_sort_info() {
  update_arr_size();
  update_array();

  if (m_ui->resultsTable->item(0, 0)->checkState()) {
    update_simple2();
  } else {
    clear_sort_info(0);
  }
  if (m_ui->resultsTable->item(1, 0)->checkState()) {
    update_simple1();
  } else {
    clear_sort_info(1);
  }
  if (m_ui->resultsTable->item(2, 0)->checkState()) {
    update_natural2();
  } else {
    clear_sort_info(2);
  }
  if (m_ui->resultsTable->item(3, 0)->checkState()) {
    update_natural1();
  } else {
    clear_sort_info(3);
  }
}

void ui::Main::config_slots() {
  connect(m_ui->exitBtn, SIGNAL(clicked()), this, SLOT(close()));
  connect(m_ui->sortBtn, SIGNAL(clicked()), this, SLOT(update_sort_info()));
  connect(m_ui->arrSize, SIGNAL(valueChanged(int)), this,
          SLOT(update_arr_size()));
}