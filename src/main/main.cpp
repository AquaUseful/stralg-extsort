#include <algorithm>
#include <array>
#include <cstddef>
#include <cstdlib>
#include <deque>
#include <forward_list>
#include <functional>
#include <iostream>
#include <list>
#include <random>
#include <vector>

#include "natural_1phase.hpp"
#include "natural_2phase.hpp"
#include "simple_1phase.hpp"
#include "simple_2phase.hpp"
#include "utils.hpp"

int main() {
  /* std::array<int, 8> inp{0, 1, 2, 0, 1, 9, -1, 7};
   std::array<int, 8> out{};

   auto oi1 = out.begin();
   auto oi2 = out.rbegin();

   sorts::detail::distribute_series(inp.begin(), inp.end(), oi1, oi2);*/

  /*std::array<int, 5> a1{0, 1, 0, 1, 9};
  std::array<int, 5> a2{2, 4, 5, 4, 10};
  std::array<int, 10> res{};

  auto i1 = a1.begin();
  auto i2 = a2.begin();
  auto o = res.begin();

  sorts::detail::merge_single_series(i1, a1.end(), i2, a2.end(), o);
  sorts::detail::merge_single_series(i1, a1.end(), i2, a2.end(), o);*/

  const std::size_t seqs = 50;

  std::random_device dev{};
  std::mt19937 gen{dev()};
  //std::vector<int> v{1, 0, 3, 4, 2};
   std::vector<int> v(seqs);
   int a{0};
   for (auto& el : v) {
     el = a;
     ++a;
   }
   std::shuffle(v.begin(), v.end(), gen);

  std::forward_list<int> seq(seqs);
  std::copy(v.begin(), v.end(), seq.begin());

  for (const auto& el : seq) {
    std::cout << el << ' ';
  }
  std::cout << '\n';

  // using IIt1 = typename decltype(seq)::iterator;
  auto stats = sorts::natural_1phase(seq.begin(), seq.end());

  for (const auto& el : seq) {
    std::cout << el << ' ';
  }
  std::cout << '\n';
  std::cout << stats.comparisons() << ' ' << stats.assignments() << '\n';

  return EXIT_SUCCESS;
}
